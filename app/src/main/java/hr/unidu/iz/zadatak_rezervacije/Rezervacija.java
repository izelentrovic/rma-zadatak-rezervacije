package hr.unidu.iz.zadatak_rezervacije;

import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.gson.Gson;

public class Rezervacija {
    private int pin, brojOsoba;
    private String restoran, datum, vrijeme, imeOsobe;

    public Rezervacija() {
    }

    public Rezervacija(int pin, int brojOsoba, String restoran, String datum, String vrijeme, String imeOsobe) {
        this.pin = pin;
        this.brojOsoba = brojOsoba;
        this.restoran = restoran;
        this.datum = datum;
        this.vrijeme = vrijeme;
        this.imeOsobe = imeOsobe;
    }


    /**
     * Getters and setters
      */

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public int getBrojOsoba() {
        return brojOsoba;
    }

    public void setBrojOsoba(int brojOsoba) {
        this.brojOsoba = brojOsoba;
    }

    public String getRestoran() {
        return restoran;
    }

    public void setRestoran(String restoran) {
        this.restoran = restoran;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getVrijeme() {
        return vrijeme;
    }

    public void setVrijeme(String vrijeme) {
        this.vrijeme = vrijeme;
    }

    public String getImeOsobe() {
        return imeOsobe;
    }

    public void setImeOsobe(String imeOsobe) {
        this.imeOsobe = imeOsobe;
    }
}
