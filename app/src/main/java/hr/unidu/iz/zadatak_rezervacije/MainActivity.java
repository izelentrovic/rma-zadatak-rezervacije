package hr.unidu.iz.zadatak_rezervacije;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {
    Intent intent;
    ImageButton btnDodaj, btnIzmijeni, btnUkloni, btnCitaj;
    TextView pickDate, pickTime;
    EditText editPin, editRestoran, editBrOsoba, editNaIme;
    Gson gson;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    private enum requestCodes {
        UNOS(1);

        private final int code;

        requestCodes(int code){
            this.code = code;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gson = new Gson();
        sharedPreferences = this.getSharedPreferences("rezervacija", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        btnDodaj = findViewById(R.id.imageButtonDodaj);
        btnIzmijeni = findViewById(R.id.imageButtonIzmijeni);
        btnUkloni = findViewById(R.id.imageButtonUkloni);
        btnCitaj = findViewById(R.id.imageButtonCitaj);
        pickDate = findViewById(R.id.textViewPickDatum);
        pickTime = findViewById(R.id.textViewPickVrijeme);
        editPin = findViewById(R.id.editTextPin);
        editRestoran = findViewById(R.id.editTextRestoran);
        editBrOsoba = findViewById(R.id.editTextBrOsoba);
        editNaIme = findViewById(R.id.editTextNaIme);

        btnDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(v.getContext(), UnosRezervacije.class);
                startActivityForResult(intent, requestCodes.UNOS.code);
            }
        });

        btnIzmijeni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String datum = pickDate.getText().toString();
                String vrijeme = pickTime.getText().toString();
                String restoran = editRestoran.getText().toString();
                int brojOsoba = Integer.parseInt(editBrOsoba.getText().toString());
                String naIme = editNaIme.getText().toString();

                int pin;
                try {
                    pin = Integer.parseInt(editPin.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "Unesite ispravan PIN!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (restoran.equals("") || brojOsoba <= 0 || naIme.equals("") || datum.equals(getString(R.string.text_pick_datum)) || vrijeme.equals(getString(R.string.text_pick_vrijeme))) {
                    Toast.makeText(getApplicationContext(), "Unesite sve podatke", Toast.LENGTH_SHORT).show();
                } else if (!sharedPreferences.contains(String.valueOf(pin))) {
                    Toast.makeText(getApplicationContext(), "Rezervacija s PINom " + pin + " ne postoji!", Toast.LENGTH_SHORT).show();
                } else {
                    pin = Integer.parseInt(editPin.getText().toString());

                    // write to XML file
                    Rezervacija rezervacija = new Rezervacija(pin, brojOsoba, restoran, datum, vrijeme, naIme);
                    editor.putString(String.valueOf(pin), gson.toJson(rezervacija));
                    editor.commit();

                    Toast.makeText(getApplicationContext(), "Rezervacija s PINom " + pin + " je izmijenjena!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnUkloni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pin;
                try {
                    pin = Integer.parseInt(editPin.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "Unesite ispravan PIN!", Toast.LENGTH_SHORT).show();
                    return;
                }
                String strPin = String.valueOf(pin);

                if (!sharedPreferences.contains(String.valueOf(pin))) {
                    Toast.makeText(getApplicationContext(), "Rezervacija s PINom " + strPin + " ne postoji!", Toast.LENGTH_SHORT).show();
                    return;
                }

                editor.remove(strPin);
                editor.commit();
                Toast.makeText(getApplicationContext(), "Rezervacija s PINom " + strPin + " je uklonjena!", Toast.LENGTH_SHORT).show();
                editRestoran.setText("");
                pickDate.setText("");
                pickTime.setText("");
                editBrOsoba.setText("");
                editNaIme.setText("");
                editPin.setText("");

                editPin.requestFocus();
            }
        });

        btnCitaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sharedPreferences.contains(String.valueOf(editPin.getText().toString()))) {
                    Toast.makeText(getApplicationContext(), "Rezervacija s PINom " + editPin.getText().toString() + " ne postoji!", Toast.LENGTH_SHORT).show();
                    return;
                }

                Rezervacija rezervacija;
                try {
                    rezervacija = fetchData(Integer.parseInt(editPin.getText().toString()));
                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "Unesite ispravan pin!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (rezervacija != null) {
                    editRestoran.setText(rezervacija.getRestoran());
                    pickDate.setText(rezervacija.getDatum());
                    pickTime.setText(rezervacija.getVrijeme());
                    editBrOsoba.setText(String.valueOf(rezervacija.getBrojOsoba()));
                    editNaIme.setText(rezervacija.getImeOsobe());
                    Toast.makeText(getApplicationContext(), "Učitavanje uspješno!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        pickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "datePicker");
            }
        });

        pickTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "timePicker");
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == requestCodes.UNOS.code) {
            if (resultCode == RESULT_OK) {
                editPin.setText(data.getStringExtra("pin"));
                editRestoran.setText(data.getStringExtra("restoran"));
                pickDate.setText(data.getStringExtra("datum"));
                pickTime.setText(data.getStringExtra("vrijeme"));
                String d = data.getStringExtra("brOsoba");
                editBrOsoba.setText(d);
                editNaIme.setText(data.getStringExtra("naIme"));
            } else if (resultCode == RESULT_CANCELED) {
              Toast.makeText(this, "Dodavanje otkazano", Toast.LENGTH_SHORT).show();
            }
        }
    }


    /**
     * Fetches data from storage into object
     * using the provided PIN
     * @param pin key of the object
     * @return deserialized Rezervacija
     */
    public Rezervacija fetchData(int pin) {
        Rezervacija rezervacija = gson.fromJson(sharedPreferences.getString(String.valueOf(pin),""), Rezervacija.class);
        if (rezervacija == null){
            Toast.makeText(this, "Rezervacija ne postoji!", Toast.LENGTH_SHORT).show();
            return null;
        }
        return rezervacija;
    }
}
