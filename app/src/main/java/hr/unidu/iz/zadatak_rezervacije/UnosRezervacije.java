package hr.unidu.iz.zadatak_rezervacije;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Random;

public class UnosRezervacije extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unos_rezervacije);

        final Intent intent = getIntent();

        final TextView pickDate = findViewById(R.id.textViewPickDatum);
        final TextView pickTime = findViewById(R.id.textViewPickVrijeme);
        final EditText editRestoran = findViewById(R.id.editTextRestoran);
        final EditText editBrOsoba = findViewById(R.id.editTextBrOsoba);
        final EditText editNaIme = findViewById(R.id.editTextNaIme);

        ImageButton btnPotvrdi = findViewById(R.id.imageButtonPotvrdi);
        ImageButton btnOtkazi = findViewById(R.id.imageButtonOtkazi);

        btnPotvrdi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String datum = pickDate.getText().toString();
                String vrijeme = pickTime.getText().toString();
                String restoran = editRestoran.getText().toString();
                int brojOsoba = Integer.parseInt(editBrOsoba.getText().toString());
                String naIme = editNaIme.getText().toString();

                if (restoran.equals("") || brojOsoba <= 0 || naIme.equals("") || datum.equals(getString(R.string.text_pick_datum)) || vrijeme.equals(getString(R.string.text_pick_vrijeme))) {
                    Toast.makeText(getApplicationContext(), "Unesite sve podatke", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences sharedPreferences = getSharedPreferences("rezervacija", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    Gson gson = new Gson();
                    int pin;
                    Random random = new Random();

                    // Generate a random number (1000-9999) that doesn't already exist
                    // Note: bad algorithm complexity
                    do {
                        pin = random.nextInt(8999) + 1001;
                    }while(sharedPreferences.contains(String.valueOf(pin)));

                    // write to XML file
                    Rezervacija rezervacija = new Rezervacija(pin, brojOsoba, restoran, datum, vrijeme, naIme);
                    editor.putString(String.valueOf(pin), gson.toJson(rezervacija));
                    editor.commit();

                    String strPin = String.valueOf(pin);
                    // Send data to MainActivity
                    intent.putExtra("pin", strPin);
                    intent.putExtra("restoran", restoran);
                    intent.putExtra("datum", datum);
                    intent.putExtra("vrijeme", vrijeme);
                    intent.putExtra("brOsoba", String.valueOf(brojOsoba));
                    intent.putExtra("naIme", naIme);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });

        btnOtkazi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });

        pickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "datePicker");
            }
        });

        pickTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "timePicker");
            }
        });
    }
}
