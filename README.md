ImageButton icons taken from [MaterialIO](https://material.io/resources/icons "Go to: https://material.io/resources/icons")

Licensed under [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0.html "Go to: https://www.apache.org/licenses/LICENSE-2.0.html")
